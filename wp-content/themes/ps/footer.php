<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Example
 */
?>

<section class="footer-top">
    <div class="thm-container">
        <div class="clearfix">
            <div class="col-md-4 hidden-sm hidden-xs pull-left img-box">
                <img src="<?php echo get_bloginfo('template_url'); ?>/images/footer-man.png" alt=""/>
            </div>
            <div class="col-md-7 right-text pull-right">
                <div class="box">
                    <div class="text-box">
                        <h3>Become a dealer</h3>
                        <p>Get special puncture service for your tyres<br /> from our experts.</p>
                        <p><span class="number">+91 965 467 6910</span> <b>OR</b> <a href="contact.html">Contact Us</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<footer id="footer" class="sec-padding">
    <div class="thm-container">
        <div class="row">
            <div class="col-md-3 col-sm-6 footer-widget">
                <div class="about-widget">
                    <a href="#"><img src="<?php echo get_bloginfo('template_url'); ?>/images/footer-logo.png" alt=""/></a>
                    <p>Puncture Solution is a leading manufacturer, supplier and exporter of Tyre and Tube repair Patches and conveyor belt repair/splicing systems. Puncture Solution was started in 2013.</p>
                    <a href="about.html">Read More <i class="fa fa-angle-double-right"></i></a>
                    <ul class="social">
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 footer-widget">
                <div class="pl-30">
                    <div class="title">
                        <h3><span>Our Products</span></h3>
                    </div>
                    <ul class="link-list">
                        <li><a href="single-service.html">PunctureSolution 250ML</a></li>
                        <li><a href="single-service.html">PunctureSolution 500ML</a></li>
                        <li><a href="single-service.html">PunctureSolution  1 Ltr</a></li>
                        <li><a href="single-service.html">PunctureSolution 20 Ltr </a></li>
                        <li><a href="single-service.html">PunctureSolution </a></li>
                        <li><a href="single-service.html">PunctureSolution  50 ML</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-2 col-sm-6 footer-widget">
                <div class="title">
                    <h3><span>Quick Links</span></h3>
                </div>
                <ul class="link-list">
                    <li><a href="index.html">Home</a></li>
                    <li><a href="about.html">About Us</a></li>
                    <li><a href="faq.html">Faqs</a></li>
                    <li><a href="gallery.html">Gallery</a></li>
                    <li><a href="contact.html">Contact Us</a></li>
                </ul>
            </div>
            <div class="col-md-4 col-sm-6 footer-widget">
                <div class="title">
                    <h3><span>Get In Touch</span></h3>
                </div>
                <ul class="contact-infos">
                    <li>
                        <div class="icon-box">
                            <i class="fa fa-map-marker"></i>
                        </div>
                        <div class="text-box">
                            <p><b>Puncture Solution PVT. LTD.</b> <br> WZ 74A, Ramgarh colony, Street no 9, Near Moti Nagar, 110015</p>
                        </div>
                    </li>
                    <li>
                        <div class="icon-box">
                            <i class="fa fa-phone"></i>
                        </div>
                        <div class="text-box">
                            <p><span class="sbold italic">+91 965 467 6910</span></p>
                        </div>
                    </li>
                    <li>
                        <div class="icon-box">
                            <i class="fa fa-envelope-o"></i>
                        </div>
                        <div class="text-box">
                            <p>info@puncturesolution.co.in</p>
                        </div>
                    </li>
                    <li>
                        <div class="icon-box">
                            <i class="icon icon-Timer"></i>
                        </div>
                        <div class="text-box">
                            <p>Monday - Friday : <span class="sbold">8.00 - 19.00</span></p>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>

<section class="bottom-bar">
    <div class="thm-container clearfix">
        <div class="pull-left">
            <p>Copyright &copy; Puncture Solution PVT. LTD. 2016. All rights reserved.</p>
        </div>
        <div class="pull-right">
            <p>Created by: Coingenesis</p>
        </div>
    </div>
</section>


<!-- jQuery js -->
<script src="<?php echo get_bloginfo('template_url'); ?>/plugins/jquery/jquery-1.11.3.min.js"></script>
<!-- bootstrap js -->
<script src="<?php echo get_bloginfo('template_url'); ?>/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- jQuery ui js -->
<script src="<?php echo get_bloginfo('template_url'); ?>/plugins/jquery-ui-1.11.4/jquery-ui.js"></script>
<!-- owl carousel js -->
<script src="<?php echo get_bloginfo('template_url'); ?>/plugins/owl.carousel-2/owl.carousel.min.js"></script>
<!-- jQuery appear -->
<script src="<?php echo get_bloginfo('template_url'); ?>/plugins/jquery-appear/jquery.appear.js"></script>
<!-- jQuery countTo -->
<script src="<?php echo get_bloginfo('template_url'); ?>/plugins/jquery-countTo/jquery.countTo.js"></script>
<!-- jQuery validation -->
<script src="<?php echo get_bloginfo('template_url'); ?>/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
<!-- gmap.js helper -->
<script src="http://maps.google.com/maps/api/js"></script>
<!-- gmap.js -->
<script src="<?php echo get_bloginfo('template_url'); ?>/plugins/gmap.js"></script>
<!-- mixit up -->
<script src="<?php echo get_bloginfo('template_url'); ?>/plugins/jquery.mixitup.min.js"></script>
<!-- fancy box -->
<script src="<?php echo get_bloginfo('template_url'); ?>/plugins/fancyapps-fancyBox/source/jquery.fancybox.pack.js"></script>
<!-- type script -->
<script src="<?php echo get_bloginfo('template_url'); ?>/plugins/typed.js-master/dist/typed.min.js"></script>
<!-- theme custom js  -->
<script src="<?php echo get_bloginfo('template_url'); ?>/js/main.js"></script>
<script type="text/javascript" src="<?php echo get_bloginfo('template_url'); ?>/js/jquery.uberAccordion.js"></script>
<script type="text/javascript">
 $('.accordionContainer').uberAccordion({
    headerClass: 'title',
    contentClass: 'content' 
  });
</script>
<?php wp_footer(); ?>
</body>
</html>







