<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * 
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>> <!--<![endif]-->
    <head>
        <meta charset="UTF-8">
        <title>Puncture Solution India, Tyre Sealant Manufacturer, Tyre Puncture Sealant India</title>
        <!-- viewport meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="<?php echo get_bloginfo('template_url'); ?>/images/favicon.png">   
        <?php wp_head(); ?>
 
        <!-- styles -->
        <!-- helpers -->
        <link rel="stylesheet" href="<?php echo get_bloginfo('template_url'); ?>/plugins/bootstrap/css/bootstrap.min.css">
        <!-- fontawesome css -->
        <link rel="stylesheet" href="<?php echo get_bloginfo('template_url'); ?>/plugins/font-awesome/css/font-awesome.min.css">
        <!-- strock gap icons -->
        <link rel="stylesheet" href="<?php echo get_bloginfo('template_url'); ?>/plugins/Stroke-Gap-Icons-Webfont/style.css">
        <!-- flaticon css -->
        <link rel="stylesheet" href="<?php echo get_bloginfo('template_url'); ?>/plugins/flaticon/flaticon.css">
        <!-- jQuery ui css -->
        <link href="<?php echo get_bloginfo('template_url'); ?>/plugins/jquery-ui-1.11.4/jquery-ui.css" rel="stylesheet">
        <!-- owl carousel css -->
        <link rel="stylesheet" href="<?php echo get_bloginfo('template_url'); ?>/plugins/owl.carousel-2/assets/owl.carousel.css">
        <link rel="stylesheet" href="<?php echo get_bloginfo('template_url'); ?>/plugins/owl.carousel-2/assets/owl.theme.default.min.css">
        <!-- animate css -->
        <link rel="stylesheet" href="<?php echo get_bloginfo('template_url'); ?>/plugins/animate.min.css">
        <!-- hover css -->
        <link rel="stylesheet" href="<?php echo get_bloginfo('template_url'); ?>/plugins/hover.css">
        <!-- fancybox -->
        <link rel="stylesheet" href="<?php echo get_bloginfo('template_url'); ?>/plugins/fancyapps-fancyBox/source/jquery.fancybox.css">
        <!-- master stylesheet -->
        <link rel="stylesheet" href="<?php echo get_bloginfo('template_url'); ?>/css/style.css">
        <!-- responsive stylesheet -->
        <link rel="stylesheet" href="<?php echo get_bloginfo('template_url'); ?>/css/responsive.css">
    </head>
    <body>
        <section class="top-bar">
	<div class="thm-container clearfix">
		<div class="left-info pull-left">
			<ul class="list-inline">    
            	<li><a class="make-app-link" href="request-qoute.html">Make Appointment</a></li>     				
				<li><a href="#"><i class="fa fa-envelope"></i> info@puncturesolution.co.in</a></li>
			</ul>
		</div>
		<div class="right-info pull-right">
			<p><span>Call us Now:</span> +91 9654 676 910</p>
		</div>
	</div>
</section>

        <header id="header" class="stricky">
            <div class="thm-container clearfix">
                <div class="logo pull-left">
                    <a href="index.html">
                        <img src="<?php echo get_bloginfo('template_url'); ?>/images/logo.png" alt="">
                    </a>
                </div>
                <div class="nav-holder pull-right">
                    <div class="nav-footer">
                        <?php
//                           / has_nav_menu('Primary');
                        if (function_exists('has_nav_menu') && has_nav_menu('primary')) {

                            wp_nav_menu(array('depth' => 2, 'sort_column' => 'menu_order', 'container' => 'ul', 'menu_class' => 'nav', 'menu_id' => 'primary', 'theme_location' => 'primary'));
                        } else {
                            ?>
                            <ul id="sec-nav" class="nav">
                                <?php wp_list_pages('sort_column=menu_order&depth=2&title_li=&exclude=' . get_option('themnific_nav_exclude')); ?>
                            </ul><!-- /#nav -->
                        <?php } ?>
                        <!-- <ul class="nav">
                                <li class="active">
                                        <a href="index.html">Home</a>
                                </li>
                                <li class="has-submenu">
                                        <a href="about.html">about us</a>						
                                </li>
                                <li class="has-submenu">
                                        <a href="products.html">Product</a>						
                                </li>							
                                <li class="has-submenu">
                                        <a href="gallery.html">Gallery</a>						
                                </li>
            <li class="has-submenu">
                                        <a href="faq.html">Faqs</a>						
                                </li>
                                <li class="has-submenu">
                                        <a href="#">Installation</a>						
                                </li>					
                                <li><a href="contact.html">contact us</a></li>
                        </ul>-->
                    </div>			
                </div>
            </div>
        </header>
