<?php
/**
 * The template for displaying all single posts.
 *
 * @package Example
 */
get_header();
?>
<div class="blog-container">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="posts-list">
                    <?php while (have_posts()) : the_post(); ?>
                        <article class="post format-standard clearfix">
                            <?php
                            $thumb_id = get_post_thumbnail_id($wp_query->ID);
                            $thumb_url = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
                            if (isset($thumb_url[0])) {
                                ?>
                                <a class="thumbnail-link" href="<?php echo $thumb_url[0]; ?>" rel="bookmark">
                                    <figure class="entry-thumbnail">
                                        <img src="<?php echo $thumb_url[0]; ?>" class="attachment-post-thumbnail wp-post-image" alt="featured_image_5">
                                        <aside class="entry-aside">
                                            <div class="side_bx">
                                                <?php example_posted_on(get_the_ID()); ?>
                                            </div>

                                            <?php post_share(); ?>
                                        </aside>
                                    </figure>
                                </a>
                            <?php } ?>
                            <div class="post-content-wrapper clearfix">
                                <div class="post-content-area">
                                    <header class="entry-header">
                                        <h2 class="entry-title">
                                            <a href="#"><?php echo get_the_title(); ?></a>
                                        </h2>
                                    </header>                               
                                    <div class="entry-content">
                                        <?php the_content(); ?>
                                    </div>                                                           
                                </div>
                                <div class="off-meta">
                                    <?php the_post_navigation(); ?>
                                    <!--                            	<nav class="navigation post-navigation" role="navigation">
                                                                        <div class="nav-links clearfix">
                                                                          <div class="nav-previous">
                                                                            <a href="#" rel="prev"><h6><span class="genericon-previous"></span> Previous Post</h6> <h3>Keep calm and play nice whenever possible</h3></a>
                                                                          </div>
                                                                          <div class="nav-next">
                                                                          <a href="#" rel="next"><h6>Next Post <span class="genericon-next"></span></h6> <h3>And it went like that</h3></a>
                                                                          </div>
                                                                        </div> 
                                                                    </nav>-->
                                </div>
                                <?php
                                if (comments_open() || get_comments_number()) :
                                    comments_template();
                                endif;
                                ?>
                            </div>
                        </article>  
                    <?php endwhile; // End of the loop. ?>
                    <?php
                    // If comments are open or we have at least one comment, load up the comment template.
//				if ( comments_open() || get_comments_number() ) :
//					comments_template();
//				endif;
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
