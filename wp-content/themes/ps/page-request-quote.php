<?php

/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 */
get_header();
?>
<section class="inner-banner">
    <div class="thm-container clearfix">
        <h2 class="pull-left">Contact Us</h2>
        <ul class="breadcumb pull-right">
            <li><a href="/"><i class="fa fa-home"></i> Home</a></li>
            <li><span>Contact Us</span></li>
        </ul>
    </div>
</section>
<section class="inner-call-to-action">
    <div class="thm-container clearfix">
        <div class="inner-wrapper clearfix">
            <div class="left-text pull-left">
                <h3><span>24/7</span> For Technical Support Call  <span>+91 9654 676 910</span></h3>
            </div>
<!--            <div class="right-button pull-right">
                <a href="contact.html" class="thm-btn">Make Appointment <i class="fa fa-arrow-right"></i></a>
            </div>-->
        </div>
    </div>
</section>

<section class="sec-padding">
    <div class="thm-container">
        <div class="sec-title">
            <h2><span>request a quote</span></h2>
            <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam</p>
        </div>
        <div class="row">
            <div class="col-md-7 col-sm-12 col-xs-12 pull-left">
                <form action="http://vinodpal.com/demos/workshop-plus/variations/orange/include/sendemail.php" class="contact-form contact-page">
                    <p><input type="text" placeholder="Frieght Type" name="frieght-type"></p>
                    <p><input type="text" placeholder="City of Departure" name="city-of-departure"></p>
                    <p><input type="text" placeholder="Delivery City" name="delivery-city"></p>
                    <p><input type="text" placeholder="Total Gross Weight" name="total-gross-weight"></p>
                    <p><input type="text" placeholder="Dimention" name="dimention"></p>
                    <p><input type="text" placeholder="Email" name="email"></p>
                    <p><textarea name="message" placeholder="Message"></textarea></p>
                    <button type="submit" class="thm-btn">Submit Now <i class="fa fa-arrow-right"></i></button>
                </form>
            </div>
            <div class="col-md-4 hidden-sm text-center pull-right">
                <img src="<?php echo get_bloginfo('template_url'); ?>/images/request-qoute-man.png" alt="Awesome Image"/>
            </div>
        </div>
    </div>
</section>

<section class="sec-padding rqa-feature bottom-has-footer-top pt0">
    <div class="thm-container">
        <div class="row">
            <div class="col-md-4 col-sm-6 bg-1 text-center">
                <div class="single-rqa-feature">
                    <i class="fa fa-globe"></i>
                    <h3>Safe and Secure</h3>
                    <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adip isci velit, sed quia non numquam.</p>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 bg-2 text-center">
                <div class="single-rqa-feature">
                    <i class="fa fa-globe"></i>
                    <h3>Great support</h3>
                    <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adip isci velit, sed quia non numquam.</p>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 bg-3 text-center">
                <div class="single-rqa-feature">
                    <i class="fa fa-globe"></i>
                    <h3>fast delivery</h3>
                    <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adip isci velit, sed quia non numquam.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>