<?php
/*
  Template Name: Gallery Template
 * Version: 1.2.2
 */
?>
<?php get_header(); ?>
<section class="inner-banner">
    <div class="thm-container clearfix">
        <h2 class="pull-left">Gallery</h2>
        <ul class="breadcumb pull-right">
            <li><a href="index.html"><i class="fa fa-home"></i> Home</a></li>
            <li><span>Our Product Gallery</span></li>
        </ul>
    </div>
</section>

<section class="inner-call-to-action">
    <div class="thm-container clearfix">
        <div class="inner-wrapper clearfix">
            <div class="left-text pull-left">
                <h3><span>24/7</span> For Technical Support Call  <span>+91 9654 676 910</span></h3>
            </div>
            <div class="right-button pull-right">
                <a href="/request-quote" class="thm-btn">Make Appointment <i class="fa fa-arrow-right"></i></a>
            </div>
        </div>
    </div>
</section>

<section class="fleet-gallery  gray-bg sec-padding">
    <div class="thm-container">
        <div class="clearfix single-fleet-gallery-wrapper">
            <?php
            global $post, $wp_query;
            $args = array(
                'post_type' => 'gallery',
                'post_status' => 'publish',
                'name' => "gallery",
                'posts_per_page' => 1
            );
            $second_query = new WP_Query($args);
            $gllr_options = get_option('gllr_options');
            $gllr_download_link_title = addslashes(__('Download high resolution image', 'gallery'));
            if ($second_query->have_posts()) {
                while ($second_query->have_posts()) : $second_query->the_post();
                    ?>

                    <!--                    <div class="gallery_box_single entry-content">-->
                    <?php if (!post_password_required()) { ?>
                        <?php
                        the_content();
                        $posts = get_posts(array(
                            "showposts" => -1,
                            "what_to_show" => "posts",
                            "post_status" => "inherit",
                            "post_type" => "attachment",
                            "orderby" => $gllr_options['order_by'],
                            "order" => $gllr_options['order'],
                            "post_mime_type" => "image/jpeg,image/gif,image/jpg,image/png",
                            "post_parent" => $post->ID
                        ));
                        if (count($posts) > 0) {
                            $count_image_block = 0;
                            ?>
                            <div class="gallery clearfix">
                                <?php
                                foreach ($posts as $attachment) {
                                    //print_r(wp_get_attachment_image_src($attachment->ID, 'full'));die;
                                    $key = "gllr_image_text";
                                    $link_key = "gllr_link_url";
                                    $alt_tag_key = "gllr_image_alt_tag";
                                    $image_attributes = wp_get_attachment_image_src($attachment->ID, 'photo-thumb');
                                    $image_attributes_large = wp_get_attachment_image_src($attachment->ID, 'large');
                                    $image_attributes_full = wp_get_attachment_image_src($attachment->ID, 'full');
                                    if (1 == $gllr_options['border_images']) {
                                        $gllr_border = 'border-width: ' . $gllr_options['border_images_width'] . 'px; border-color:' . $gllr_options['border_images_color'] . '';
                                        $gllr_border_images = $gllr_options['border_images_width'] * 2;
                                    } else {
                                        $gllr_border = '';
                                        $gllr_border_images = 0;
                                    }
                                    // if ($count_image_block % $gllr_options['custom_image_row_count'] == 0) {
                                    ?>
                                    <!--                                            <div class="gllr_image_row">-->
                                    <?php //} ?>
                                    <div class="col-md-4 col-sm-6 col-xs-12 mix ground">
                                        <div class="single-fleet-gallery img-cap-effect">
                                            <div class="img-box">
                                                <img src="<?php echo $image_attributes_full[0]; ?>" alt="<?php echo get_post_meta( $attachment->ID, $alt_tag_key, true ); ?>" title="<?php echo get_post_meta( $attachment->ID, $key, true ); ?>"/>
                                                <div class="img-caption">
                                                    <div class="box-holder">
                                                        <div class="box-caption">									
                                                            <ul>
                                                                <li><a href="<?php echo $image_attributes_full[0]; ?>" class="fancybox"><i class="fa fa-search-plus"></i></a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    //}
                                    $count_image_block++;
                                }
                                if ($count_image_block > 0 && $count_image_block % $gllr_options['custom_image_row_count'] != 0) {
                                    ?>
                                </div><!-- .gllr_image_row -->
                            <?php } ?>
                            <!-- .</div>gallery.clearfix -->
                        <?php } ?>
                    <?php } else { ?>
                        <p><?php echo get_the_password_form(); ?></p>
                        <?php
                    }
                endwhile;
                if (1 == $gllr_options['return_link']) {
                    if ('gallery_template_url' == $gllr_options["return_link_page"]) {
                        global $wpdb;
                        $parent = $wpdb->get_var("SELECT $wpdb->posts.ID FROM $wpdb->posts, $wpdb->postmeta WHERE meta_key = '_wp_page_template' AND meta_value = 'gallery-template.php' AND (post_status = 'publish' OR post_status = 'private') AND $wpdb->posts.ID = $wpdb->postmeta.post_id");
                        ?>
                        <div class="return_link"><a href="<?php echo (!empty($parent) ? get_permalink($parent) : '' ); ?>"><?php echo $gllr_options['return_link_text']; ?></a></div>
                    <?php } else { ?>
                        <div class="return_link"><a href="<?php echo $gllr_options["return_link_url"]; ?>"><?php echo $gllr_options['return_link_text']; ?></a></div>
                        <?php
                    }
                }
            } else {
                ?>
                <div class="gallery_box_single">
                    <p class="not_found"><?php _e('Sorry, nothing found.', 'gallery'); ?></p>
                <?php } ?>				
            </div><!-- .gallery_box_single -->			
        </div>
    </div>
</section>
<!--<script type="text/javascript">
    (function($){
    $(document).ready(function(){
    $("a[rel=gallery_fancybox<?php //if (0 == $gllr_options['single_lightbox_for_multiple_galleries']) echo '_' . $post->ID; ?>]").fancybox({
    'transitionIn'			: 'elastic',
            'transitionOut'			: 'elastic',
            'titlePosition' 		: 'inside',
            'speedIn'				:	500,
            'speedOut'				:	300,
            'titleFormat'			: function(title, currentArray, currentIndex, currentOpts) {
            return '<div id="fancybox-title-inside">' + (title.length ? '<span id="bws_gallery_image_title">' + title + '</span><br />' : '') + '<span id="bws_gallery_image_counter"><?php //_e("Image", "gallery"); ?> ' + (currentIndex + 1) + ' / ' + currentArray.length + '</span></div><?php if (get_post_meta($post->ID, 'gllr_download_link', true) != '') { ?><a id="bws_gallery_download_link" href="' + $(currentOpts.orig).attr('rel') + '" target="_blank"><?php echo $gllr_download_link_title; ?> </a><?php } ?>';
            }<?php //if ($gllr_options['start_slideshow'] == 1) { ?>,
                'onComplete':	function() {
                clearTimeout(jQuery.fancybox.slider);
                        jQuery.fancybox.slider = setTimeout("jQuery.fancybox.next()",<?php //echo empty($gllr_options['slideshow_interval']) ? 2000 : $gllr_options['slideshow_interval']; ?>);
                }<?php //} ?>
    });
    });
    })(jQuery);
</script>-->
<?php get_footer(); ?>