<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package Example
 */
get_header();
?>
<div class="blog-container">
    <div class="container">
        <div class="row">   
            <header class="page-header">
                        <h1 class="page-title"><?php esc_html_e('Oops! That page can&rsquo;t be found.', 'example'); ?></h1>
                    </header><!-- .page-header -->
            <div class="col-lg-8 col-md-9">
                
                    

                    <div class="page-content">
                        <p><?php esc_html_e('It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'example'); ?></p>

                        <?php get_search_form(); ?>
                        
                    </div>
            </div>
                    <div class="col-lg-4 col-md-3">
                        <?php get_sidebar(); ?>
                    </div>
            </div>

        </div>
    </div>
    <?php get_footer(); ?>
