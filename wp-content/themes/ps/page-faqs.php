<?php get_header(); 
//if ( get_query_var('q') )
//{
  //print_r(get_query_var('q'));die;
?>
<section class="inner-banner">
	<div class="thm-container clearfix">
		<h2 class="pull-left">FAQ</h2>
		<ul class="breadcumb pull-right">
			<li><a href="index.html"><i class="fa fa-home"></i> Home</a></li>
			<li><span>FAQ</span></li>
		</ul>
	</div>
</section>

<section class="inner-call-to-action">
	<div class="thm-container clearfix">
		<div class="inner-wrapper clearfix">
			<div class="left-text pull-left">
				<h3><span>24/7</span> For Technical Support Call  <span>+91 9654 676 910</span></h3>
			</div>
			<div class="right-button pull-right">
				<a href="contact.html" class="thm-btn">Contact Us <i class="fa fa-arrow-right"></i></a>
			</div>
		</div>
	</div>
</section>

<section class="sec-padding faq-page bottom-has-footer-top">
	<div class="thm-container">
		<div class="row">			
			<div class="col-md-12">
				<div class="sec-title">
					<h2><span>Frequently ask questions</span></h2>					
				</div>
                            <?php if ( have_posts() ) : ?>

			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php the_content(); ?>

			<?php endwhile; ?>

			<?php the_posts_navigation(); ?>

		<?php else : ?>

			<?php get_template_part( 'template-parts/content', 'none' ); ?>

		<?php endif; ?>
                            </div>
		</div>
	</div>
</section>
<?php 
//}
get_footer(); ?>