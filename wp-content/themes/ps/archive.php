<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Example
 */

get_header(); ?>
<div class="blog-container">
    <div class="container">
        <div class="row">
            <header class="page-header">
				<?php
					the_archive_title( '<h1 class="page-title">', '</h1>' );
					the_archive_description( '<div class="taxonomy-description">', '</div>' );
				?>
			</header><!-- .page-header -->

            <div class="col-lg-8 col-md-9">
                <div class="posts-list">
                   <?php if ( have_posts() ) : ?>
			
			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
                            <article class="post format-standard clearfix">
                                <?php
                                $thumb_id = get_post_thumbnail_id($wp_query->ID);
                                $thumb_url = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
                                if (isset($thumb_url[0])) {
                                    ?>
                                    <a class="thumbnail-link" href="<?php echo $thumb_url[0]; ?>" rel="bookmark">
                                        <figure class="entry-thumbnail">
                                            <img src="<?php echo $thumb_url[0]; ?>" class="attachment-post-thumbnail wp-post-image" alt="featured_image_5">
                                            <aside class="entry-aside">
                                                <div class="side_bx">
                                                    <?php example_posted_on(get_the_ID()); ?>
                                                </div>

                                                <?php post_share(); ?>
                                            </aside>
                                        </figure>
                                    </a>
                                <?php } ?>
                                <div class="post-content-area">
                                    <header class="entry-header">
                                        <h2 class="entry-title">
                                            <a href="#"><?php echo get_the_title(); ?></a>
                                        </h2>
                                        <p> 
                                            <?php
                                        //the_excerpt();
                                        global $more;    // Declare global $more (before the loop).
                                        $more = 0;
                                        ?>
        <?php the_content(''); ?>
                                        </p>
                                    </header>
                                    <div class="entry-content">
                                        <p>
                                       
                                            <a href="<?php the_permalink(); ?>" class="more-link">Read More…</a>
                                        </p>
                                    </div>
                                    <!-- .entry-content -->
                                </div>
                            </article>
                        <?php endwhile; ?>

                        <?php the_posts_navigation(); ?>

                    <?php else : ?>

                        <?php get_template_part('template-parts/content', 'none'); ?>

                    <?php endif; ?>			
                </div>
            </div>
             <div class="col-lg-4 col-md-3">
                <?php get_sidebar(); ?>
            </div>
            <!--<div class="col-lg-3 col-md-4">
                            <div class="right_bx_4 bmargin20">
                                    <div class="search_box clearfix">
                                            <div class="sr_fild">
                                                    <form>
                                                            <input type="text" placeholder="Search Blog.."/>
                                                            <input type="submit" value="" />
                                                    </form>
                                            </div>
                                    </div>
                            </div>
                            <div class="categ_box bmargin20">
                                    <h3>Categories</h3>
                                    <ul class="cate_ul">
                                            <li>
                                                    <a href="#">Categories text here</a>
                                            </li>
                                            <li>
                                                    <a href="#">lorem ipsum text here</a>
                                            </li>
                                            <li>
                                                    <a href="#">lorem ipsum text is amet</a>
                                            </li>
                                            <li>
                                                    <a href="#">lorem ipsum text is amet dummy</a>
                                            </li>
                                            <li>
                                                    <a href="#">Categories text here</a>
                                            </li>
                                    </ul>
                            </div>
                            <div class="categ_box bmargin20">
                                    <h3>Recent Posts</h3>
                                    <ul class="cate_ul">
                                            <li>
                                                    <a href="#">lorem ipsum text here</a>
                                            </li>
                                            <li>
                                                    <a href="#">lorem ipsum text is amet</a>
                                            </li>
                                            <li>
                                                    <a href="#">lorem ipsum text is amet dummy</a>
                                            </li>
                                            <li>
                                                    <a href="#">Post text here</a>
                                            </li>
                                    </ul>
                            </div>
                    </div>-->
        </div>
    </div>
</div>
<?php get_footer(); ?>