<?php get_header()?>
<?php
global $post, $wp_query;
$posts = array(
    //"showposts" => 5,
    "what_to_show" => "posts",
    "post_status" => "inherit",
    "post_type" => "attachment",
    "post_mime_type" => "image/jpeg,image/gif,image/jpg,image/png",
    "post_parent_in" => array(207, 74,181,115,138,105,223,125,219)
);
query_posts($posts);   // load posts
if (have_posts()) :
    ?>
    <?php /* Start the Loop */ ?>
    <?php
    while (have_posts()) : the_post();
        //print_r($post);
        //if (count($posts) > 0) {
        $count_image_block = 0;
        ?>
        <?php
        //foreach ($posts as $attachment) {
//                                    /echo $attachment->post_parent;
        $key = "gllr_image_text";
        $link_key = "gllr_link_url";
        $alt_tag_key = "gllr_image_alt_tag";
        $image_attributes = wp_get_attachment_image_src($post->ID, 'photo-thumb');
        $image_attributes_large = wp_get_attachment_image_src($post->ID, 'large');
        $image_attributes_full = wp_get_attachment_image_src($post->ID, '');

        $gllr_border = '';
        $gllr_border_images = 0;
        ?>



        <!--        												<div class="gllr_image_block">
                                                                                                                        <p style="width:<?php echo $gllr_options['gllr_custom_size_px'][1][0] + $gllr_border_images; ?>px;height:<?php echo $gllr_options['gllr_custom_size_px'][1][1] + $gllr_border_images; ?>px;">-->
        <?php if (( $url_for_link = get_post_meta($post->ID, $link_key, true) ) != "") { ?>
                            <!--            															<a href="<?php echo $url_for_link; ?>" title="<?php echo get_post_meta($attachment->ID, $key, true); ?>" target="_blank">
                                                                                                                                                                    <img width="<?php echo $gllr_options['gllr_custom_size_px'][1][0]; ?>" height="<?php echo $gllr_options['gllr_custom_size_px'][1][1]; ?>" style="width:<?php echo $gllr_options['gllr_custom_size_px'][1][0]; ?>px; height:<?php echo $gllr_options['gllr_custom_size_px'][1][1]; ?>px; <?php echo $gllr_border; ?>" alt="<?php echo get_post_meta($attachment->ID, $alt_tag_key, true); ?>" title="<?php echo get_post_meta($attachment->ID, $key, true); ?>" src="<?php echo $image_attributes[0]; ?>" />
                                                                                                                                                            </a>-->
        <?php } else { ?>
            <div class="grid-item images">
                <a href="<?php echo $image_attributes_full[0]; ?>" title="<?php echo get_post_meta($post->ID, $key, true); ?>"><img alt="<?php echo get_post_meta($post->ID, $alt_tag_key, true); ?>" title="<?php echo get_post_meta($attachment->ID, $key, true); ?>" src="<?php echo $image_attributes_full[0]; ?>" /></a>
            </div>                                
            <!--                                                                                                                    <a rel="gallery_fancybox<?php if (0 == $gllr_options['single_lightbox_for_multiple_galleries']) echo '_' . $post->ID; ?>" href="<?php echo $image_attributes_large[0]; ?>" title="<?php echo get_post_meta($post->ID, $key, true); ?>" >
                                                                                                    <img width="<?php echo $gllr_options['gllr_custom_size_px'][1][0]; ?>" height="<?php echo $gllr_options['gllr_custom_size_px'][1][1]; ?>" style="width:<?php echo $gllr_options['gllr_custom_size_px'][1][0]; ?>px; height:<?php echo $gllr_options['gllr_custom_size_px'][1][1]; ?>px; <?php echo $gllr_border; ?>" alt="<?php echo get_post_meta($post->ID, $alt_tag_key, true); ?>" title="<?php echo get_post_meta($attachment->ID, $key, true); ?>" src="<?php echo $image_attributes[0]; ?>" rel="<?php echo $image_attributes_full[0]; ?>" />
                                                                                            </a>-->
        <?php } ?>									
        <!--        													</p>-->

        <!--        												</div> .gllr_image_block -->
        <?php
        $count_image_block++;
    endwhile;
endif;
?>
<?php get_footer();?>