<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Example
 */
get_header();
?>
<?php echo rc_slider_shortcode(); ?>  
<!--<section class="rev_slider_wrapper thm-banner-wrapper">
	<div id="slider1" class="rev_slider"  data-version="5.0">
		<ul>

			<li data-transition="parallaxvertical" data-ease="SlowMo.ease">
				<img src="<?php echo get_bloginfo('template_url'); ?>/images/slider/1.jpg"  alt="">
				<div class="tp-caption sfb tp-resizeme caption-h1 light-bg" 
			        data-x="right" data-hoffset="122" 
			        data-y="top" data-voffset="180" 
			        data-whitespace="nowrap"
			        data-transform_idle="o:1;" 
			        data-transform_in="o:0" 
			        data-transform_out="o:0" 
			        data-start="500">
					We will take you
			    </div>
				<div class="tp-caption sfb tp-resizeme caption-h1 dark-bg" 
			        data-x="right" data-hoffset="10" 
			        data-y="top" data-voffset="243" 
			        data-whitespace="nowrap"
			        data-transform_idle="o:1;" 
			        data-transform_in="o:0" 
			        data-transform_out="o:0" 
			        data-start="500">
					WHEREVER YOU NEED
			    </div>				
				<div class="tp-caption sfl tp-resizeme" 
			        data-x="right" data-hoffset="305" 
			        data-y="top" data-voffset="350" 
			        data-whitespace="nowrap"
			        data-transform_idle="o:1;" 
			        data-transform_in="o:0" 
			        data-transform_out="o:0" 
			        data-start="1500">
					<a href="contact.html" class="thm-btn">Contact Now <i class="fa fa-arrow-right"></i></a>
			    </div>
				<div class="tp-caption sfr tp-resizeme" 
			        data-x="right" data-hoffset="110" 
			        data-y="top" data-voffset="350" 
			        data-whitespace="nowrap"
			        data-transform_idle="o:1;" 
			        data-transform_in="o:0" 
			        data-transform_out="o:0" 
			        data-start="2000">
					<a href="products.html" class="thm-btn transparent">View Products <i class="fa fa-arrow-right"></i></a>
			    </div>
			</li>
		</ul>
	</div>
</section>-->


<section class="about-info-box sec-padding home-page">
	<div class="thm-container">
		<div class="clearfix">
			<div class="col-lg-5 hidden-md hidden-sm hidden-xs pull-left">
				<div class="left-box">
					<div class="img-box">
						<img src="<?php echo get_bloginfo('template_url'); ?>/images/1LTR.JPG" alt=""/>
					</div>
				</div>
			</div>
			<div class="col-lg-7 col-md-12 pull-right">
				<div class="right-box">
					<div class="sec-title">
						<h2><span>One Stop Shop For All Puncture Repairs</span></h2>
						<p>Puncture Solution is a leading manufacturer, supplier and exporter of Tyre and Tube repair Patches and conveyor belt repair/splicing systems. Puncture Solution was started in 2013.</p>
                        <p>Puncture Solution has the reputation of being one of the foremost Tyre repair patches manufacturers in Delhi, India who manufacture a complete range of tire & tube repair materials for any size of injury, from pinhole repairs in tubes to section repairs in Giant tires. We are also counted as one of India’s largest supplier of tire tube repair patches and also as exporters of superior quality tyre repair patches.</p>
                        <p>We are identified as one of the major Tube Puncture Repair Patches Manufacturers in India. Various national as well as international clients are placing bulk orders for the Tube Repair Patches due to the unmatched quality and unsurpassable performance in respective applications. Additionally, customers are eased with the availability of the Tube Repairing Patches as per the specifications provided in terms of sizes and dimensions.</p>
                        <p>As a company and a brand, we stand for top quality and outstanding service. Augmented by state-of-the-art technology and pioneering innovations, Puncture Solution is complimented by a team of experts to ensure that the business constantly maintains its position at the forefront of our industry.</p>						
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="featured-services">
	<div class="thm-container">
		<div class="row">
			<div class="col-lg-7 col-md-12 featured-service-box">
				<div class="text-box">
					<div class="sec-title">
						<h2><span>Welcome to Puncture Solution</span></h2>
						<p>Puncture Solution is the biggest manufacturer, Develeper, Importer, Exporter & Worldwide supplier of very Unique Special Innovative range of technology products. </p>
					</div>
					<h3>We are available for 24/7</h3>
					<ul class="dtc">
						<li><i class="fa fa-hand-o-right"></i>COMPLETE SAFETY ANALYSIS</li>
						<li><i class="fa fa-hand-o-right"></i>DRIVABILITY PROBLEMS</li>
					</ul>
					<ul class="dtc">
						<li><i class="fa fa-hand-o-right"></i>Tyres Changing</li>
						<li><i class="fa fa-hand-o-right"></i>Puncture Solution</li>
					</ul>
				</div>
			</div>
			<div class="col-lg-5 hidden-md col-sm-12">
				<div class="left-full-img">
					<img src="<?php echo get_bloginfo('template_url'); ?>/images/left-full-image.jpg" alt=""/>
				</div>
			</div>
		</div>
	</div>
</section>



<section class="team-section with-carousel sec-padding">
	<div class="thm-container">
		<div class="sec-title">
			<h2><span>Our Products</span></h2>
		</div>
		<div class="owl-carousel owl-theme">
			<div class="item">
				<div class="single-team-member">
					<div class="img-box">
						<img src="<?php echo get_bloginfo('template_url'); ?>/images/products/250ML.JPG" alt=""/>
					</div>
					<div class="content hvr-sweep-to-bottom">						
						<div class="name-box">
							<h3>PunctureSolution <br /> 250ML</h3>
							<span>390.00 MRP</span>
						</div>
					</div>
					
				</div>
			</div>
			<div class="item">
				<div class="single-team-member">
					<div class="img-box">
						<img src="<?php echo get_bloginfo('template_url'); ?>/images/products/500ML1.JPG" alt=""/>
					</div>
					<div class="content hvr-sweep-to-bottom">						
						<div class="name-box">
							<h3>PunctureSolution <br /> New 500ML</h3>
							<span>690.00 MRP</span>
						</div>
					</div>
					
				</div>
			</div>
			<div class="item">
				<div class="single-team-member">
					<div class="img-box">
						<img src="<?php echo get_bloginfo('template_url'); ?>/images/products/500ML.JPG" alt=""/>
					</div>
					<div class="content hvr-sweep-to-bottom">						
						<div class="name-box">
							<h3>PunctureSolution <br />500ML</h3>
							<span>690.00 MRP</span>
						</div>
					</div>
					
				</div>
			</div>
			<div class="item">
				<div class="single-team-member">
					<div class="img-box">
						<img src="<?php echo get_bloginfo('template_url'); ?>/images/products/1LTR.JPG" alt=""/>
					</div>
					<div class="content hvr-sweep-to-bottom">						
						<div class="name-box">
							<h3>PunctureSolution <br /> 1 LITRES </h3>
                            <span>1100.00 MRP</span>
						</div>
					</div>					
				</div>
			</div>
            <div class="item">
				<div class="single-team-member">
					<div class="img-box">
						<img src="<?php echo get_bloginfo('template_url'); ?>/images/products/20l.JPG" alt=""/>
					</div>
					<div class="content hvr-sweep-to-bottom">						
						<div class="name-box">
							<h3>PunctureSolution <br /> 20 LITRES </h3>
                            <span>20099.00 MRP</span>
						</div>
					</div>					
				</div>
			</div>
            <div class="item">
				<div class="single-team-member">
					<div class="img-box">
						<img src="<?php echo get_bloginfo('template_url'); ?>/images/products/100ml1000.JPG" alt=""/>
					</div>
					<div class="content hvr-sweep-to-bottom">						
						<div class="name-box">
							<h3>PunctureSolution <br /> 100 ML </h3>
                            <span>1000.00 MRP</span>
						</div>
					</div>					
				</div>
			</div>
            <div class="item">
				<div class="single-team-member">
					<div class="img-box">
						<img src="<?php echo get_bloginfo('template_url'); ?>/images/products/100ml1000.JPG" alt=""/>
					</div>
					<div class="content hvr-sweep-to-bottom">						
						<div class="name-box">
							<h3>PunctureSolution <br /> 200 ML </h3>
                             <span>1300.00 MRP</span>
						</div>
					</div>					
				</div>
			</div>
            <div class="item">
				<div class="single-team-member">
					<div class="img-box">
						<img src="<?php echo get_bloginfo('template_url'); ?>/images/products/50ml.JPG" alt=""/>
					</div>
					<div class="content hvr-sweep-to-bottom">						
						<div class="name-box">
							<h3>PunctureSolution <br /> 50 ML </h3>
                            <span>&nbsp;</span>
						</div>
					</div>					
				</div>
			</div>
		</div>			
	</div>
</section>


<section class="faq-section sec-padding">
	<div class="thm-container">
		<div class="row">
			<div class="col-md-8">
				<div class="sec-title">
					<h2><span>some of our core values</span></h2>
				</div>
				<div class="accrodion-grp" data-grp-name="faq-accrodion">
					<div class="accrodion active">
						<div class="accrodion-title">
							<h4>Our Mission</h4>
						</div>
						<div class="accrodion-content">
							<div class="img-caption">
								<div class="img-box">
									<img src="<?php echo get_bloginfo('template_url'); ?>/images/accrodion/1.jpg" alt=""/>
								</div>
								<div class="content-box">
									<p>A company of highly motivated agile and happy people that innovate continuously with seamless execution to provide its consumers across the country with useful and helpful products.</p>									
								</div>
							</div>
						</div>
					</div>
					<div class="accrodion ">
						<div class="accrodion-title">
							<h4>Our Vision</h4>
						</div>
						<div class="accrodion-content">
							<div class="img-caption">
								<div class="img-box">
									<img src="<?php echo get_bloginfo('template_url'); ?>/images/accrodion/1.jpg" alt=""/>
								</div>
								<div class="content-box">
									<p>We believe that in any success 10% is the contribution of an idea but 90% is making the things happen with a consistent approach.</p>
								</div>
							</div>
						</div>
					</div>
					<div class="accrodion">
						<div class="accrodion-title">
							<h4>Who We Are</h4>
						</div>
						<div class="accrodion-content">
							<div class="img-caption">
								<div class="img-box">
									<img src="<?php echo get_bloginfo('template_url'); ?>/images/accrodion/1.jpg" alt=""/>
								</div>
								<div class="content-box">
									<p>We are the people known for quality. We never compromise with quality. Quality is the main motive behind our never ending Research and Development.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="sec-title">
					<h2><span>Testimonials</span></h2>
				</div>
				<div class="testimonial-content-box owl-carousel owl-theme">
					<div class="item">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quisno strud exercitation ullamco laboris nisi ut aliquip ex ea commodo.  incididunt ut labore et dolore magna aliqu.</p>
						<span class="name">- Seema</span>
					</div>
					<div class="item">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quisno strud exercitation ullamco laboris nisi ut aliquip ex ea commodo.  incididunt ut labore et dolore magna aliqu.</p>
						<span class="name">- Mohit</span>
					</div>
					<div class="item">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quisno strud exercitation ullamco laboris nisi ut aliquip ex ea commodo.  incididunt ut labore et dolore magna aliqu.</p>
						<span class="name">- Rajesh</span>
					</div>					
				</div>
				<div class="testimonail-thumbnail-box owl-carousel owl-theme">
					<div class="item">
						<img src="<?php echo get_bloginfo('template_url'); ?>/images/testimonials/1.png" alt="">
					</div>
					<div class="item">
						<img src="<?php echo get_bloginfo('template_url'); ?>/images/testimonials/2.png" alt="">
					</div>
					<div class="item">
						<img src="<?php echo get_bloginfo('template_url'); ?>/images/testimonials/3.png" alt="">
					</div>					
				</div>
			</div>
		</div>
	</div>
</section>

<section class="fact-wrapper sec-padding">
<?php get_footer(); ?>
   
