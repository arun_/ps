<?php
/**
 * Example functions and definitions
 *
 * @package Example
 */
if (!function_exists('example_setup')) :

    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function example_setup() {
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on Example, use a find and replace
         * to change 'example' to the name of your theme in all the template files
         */
        load_theme_textdomain('example', get_template_directory() . '/languages');

        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
         */
        add_theme_support('post-thumbnails');

        // This theme uses wp_nav_menu() in one location.
        register_nav_menus(array(
            'primary' => esc_html__('Primary Menu', 'example'),
        ));

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support('html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ));

        /*
         * Enable support for Post Formats.
         * See http://codex.wordpress.org/Post_Formats
         */
        add_theme_support('post-formats', array(
            'aside',
            'image',
            'video',
            'quote',
            'link',
        ));

        // Set up the WordPress core custom background feature.
        add_theme_support('custom-background', apply_filters('example_custom_background_args', array(
            'default-color' => 'ffffff',
            'default-image' => '',
        )));
    }

endif; // example_setup
add_action('after_setup_theme', 'example_setup');

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function example_content_width() {
    $GLOBALS['content_width'] = apply_filters('example_content_width', 640);
}

add_action('after_setup_theme', 'example_content_width', 0);

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function example_widgets_init() {
    register_sidebar(array(
        'name' => esc_html__('Sidebar', 'example'),
        'id' => 'sidebar-1',
        'description' => '',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h1 class="widget-title">',
        'after_title' => '</h1>',
    ));
}

add_action('widgets_init', 'example_widgets_init');


/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';
require_once(get_template_directory() . '/inc/slider/slider_post_type.php');
require_once( get_template_directory() . '/inc/slider/slider.php' );

function ps_scripts() {
    wp_enqueue_script('jquery', get_template_directory_uri() . '/js/jquery-1.9.1.min.js', array(), '1.9.1', true);
    wp_enqueue_script('bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '1.0.0', true);
    wp_enqueue_script('masonry', get_template_directory_uri() . '/js/masonry.pkgd.min.js', array(), '1.0.0', true);
    wp_enqueue_script('imagesloaded', get_template_directory_uri() . '/js/imagesloaded.pkgd.min.js', array(), '1.0.0', true);
    wp_enqueue_script('lightgallery', get_template_directory_uri() . '/js/jquery.magnific-popup.js', array(), '1.0.0', true);
    wp_enqueue_script('slick', get_template_directory_uri() . '/js/slick.min.js', array(), '1.0.0', true);
    wp_enqueue_script('featherlight', get_template_directory_uri() . '/js/featherlight.js', '1.0.0', true);
    wp_register_script('custom', get_template_directory_uri() . '/js/custom.js', array(), '1.0.0', true);
    wp_localize_script('custom', 'myAjax', array('ajaxurl' => admin_url('admin-ajax.php')));
    wp_enqueue_script('custom');
    wp_register_script('infinite_scrolling', //name of script
            get_template_directory_uri() . '/js/jquery.infinitescroll.min.js', //where the file is
            array('jquery'), //this script requires a jquery script
            null, //don't have a script version number
            true//script will de placed on footer
    );

    //if(!is_singular()){ //only when we have more than 1 post
    //we'll load this script
    wp_enqueue_script('infinite_scrolling');
    //}
}

add_action('wp_enqueue_scripts', 'ps_scripts');
add_filter('nav_menu_css_class', 'special_nav_class', 10, 2);

function special_nav_class($classes, $item) {
    if (in_array('current-menu-item', $classes)) {
        $classes[] = 'active ';
    }
    return $classes;
}

add_action("wp_ajax_my_gallery_image", "my_gallery_image");
add_action("wp_ajax_nopriv_my_gallery_image", "my_gallery_image");

function my_gallery_image() {
    ?>

    <div class="photo-wrapper">
        <a href="javascript:void(0);" onClick="closeAlbumPreview();" class="close-btn"></a>
        <!--ALBUM CAROUSAL-->
        <div class="photo-carousel">
            <?php
            global $post, $wp_query;
            $posts = get_posts(array(
                // "showposts" => -1,
                "what_to_show" => "posts",
                "post_status" => "inherit",
                "post_type" => "attachment",
                "post_mime_type" => "image/jpeg,image/gif,image/jpg,image/png",
                "post_parent" => $_REQUEST["post_id"]
            ));
            //print_r($posts);die;
            if (count($posts) > 0) {
                $count_image_block = 0;
                foreach ($posts as $attachment) {
                    $key = "gllr_image_text";
                    $link_key = "gllr_link_url";
                    $alt_tag_key = "gllr_image_alt_tag";
                    $image_attributes = wp_get_attachment_image_src($attachment->ID, 'photo-thumb');
                    $image_attributes_large = wp_get_attachment_image_src($attachment->ID, 'large');
                    $image_attributes_full = wp_get_attachment_image_src($attachment->ID, 'full');
                    if (( $url_for_link = get_post_meta($attachment->ID, $link_key, true) ) != "") {
                        
                    } else {
                        ?>
                        <div>
                            <div class="img"><img alt="<?php echo get_post_meta($attachment->ID, $alt_tag_key, true); ?>" title="<?php echo get_post_meta($attachment->ID, $key, true); ?>" src="<?php echo $image_attributes[0]; ?>" /></div>
                            <!--<p><?php echo get_post_meta($attachment->ID, $key, true); ?> <br /><?php echo date('Y', strtotime(get_the_date('', $attachment->ID))); ?></p>-->
                        </div>
            <?php
            }
        }
    }
    ?>

        </div>
        <!--END ALBUM CAROUSAL-->
        <!--Photo Preview-->
        <div class="photo-preview-container">
            <div class="large-photo-carousel">
                <?php
                if (count($posts) > 0) {
                    $count_image_block = 0;
                    foreach ($posts as $attachment) {
                        $key = "gllr_image_text";
                        $link_key = "gllr_link_url";
                        $alt_tag_key = "gllr_image_alt_tag";
                        $image_attributes = wp_get_attachment_image_src($attachment->ID, 'photo-thumb');
                        $image_attributes_large = wp_get_attachment_image_src($attachment->ID, 'large');
                        $image_attributes_full = wp_get_attachment_image_src($attachment->ID, 'full');
                        if (( $url_for_link = get_post_meta($attachment->ID, $link_key, true) ) != "") {
                            
                        } else {
                            ?>
                            <div>
                                <img src="<?php echo $image_attributes_full[0]; ?>" />
                                <h3><?php echo get_post_meta($attachment->ID, $key, true); ?></h3>
                            </div>
            <?php
            }
        }
    }
    ?>                       
            </div>
        </div>
        <!--End Photo Preview-->
    </div>
    <script>
        jQuery('body').addClass('noscroll');
        jQuery('#preview-popup').fadeIn();
    //Album's photo carousel
        jQuery('.photo-carousel').slick({
            arrows: true,
            slidesToShow: 4,
            slidesToScroll: 1,
            focusOnSelect: true,
            asNavFor: '.large-photo-carousel',
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 700,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 520,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
        jQuery('.large-photo-carousel').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            fade: false,
            asNavFor: '.photo-carousel',
            adaptiveHeight: false,
        });
    </script>


    <?php
    die();
}

function wp_infinitepaginate() {
    global $post, $wp_query;
    $offset = 2;
    $loopFile = $_POST['loop_file'];
    $paged = $_POST['page_no'];
    $posts_per_page = get_option('posts_per_page');
    $page_offset = ($paged - 1) * 12 + 1;
    # Load the posts
    //query_posts(array('paged' => $paged));

    $posts = array(
        "showposts" => 12,
        "what_to_show" => "posts",
        "post_status" => "inherit",
        "post_type" => "attachment",
        "post_mime_type" => "image/jpeg,image/gif,image/jpg,image/png",
        "post_parent__in" => array(207, 74, 181, 115, 138, 105, 223, 125, 219),
        "offset" => $page_offset,
    );
    query_posts($posts);   // load posts
    if (have_posts()) :
        ?>
        <?php /* Start the Loop */ ?>
        <?php
        while (have_posts()) : the_post();
            //print_r($post);
            //if (count($posts) > 0) {
            $count_image_block = 0;
            ?>
            <?php
            //foreach ($posts as $attachment) {
//                                    /echo $attachment->post_parent;
            $key = "gllr_image_text";
            $link_key = "gllr_link_url";
            $alt_tag_key = "gllr_image_alt_tag";
            $image_attributes = wp_get_attachment_image_src($post->ID, 'photo-thumb');
            $image_attributes_large = wp_get_attachment_image_src($post->ID, 'large');
            $image_attributes_full = wp_get_attachment_image_src($post->ID, '');

            $gllr_border = '';
            $gllr_border_images = 0;
            ?>



            <?php if (( $url_for_link = get_post_meta($post->ID, $link_key, true) ) != "") { ?>
            <?php } else { ?>
                <div class="grid-item images">
                    <a href="<?php echo $image_attributes_full[0]; ?>" title="<?php echo get_post_meta($post->ID, $key, true); ?>"><img alt="<?php echo get_post_meta($post->ID, $alt_tag_key, true); ?>" title="<?php echo get_post_meta($post->ID, $key, true); ?>" src="<?php echo $image_attributes_large[0]; ?>" /></a>
                </div>                                
            <?php } ?>									
            <?php
            $count_image_block++;
        endwhile;
    endif;
    //get_template_part($loopFile);

    exit;
}

add_action('wp_ajax_infinite_scroll', 'wp_infinitepaginate');           // for logged in user
add_action('wp_ajax_nopriv_infinite_scroll', 'wp_infinitepaginate');

/*
  Function that will set infinite scrolling to be displayed in the page.
 */

function set_infinite_scrolling() {

    //if(!is_singular()){//again, only when we have more than 1 post
    //add js script below
    ?>    
    <script type="text/javascript">
        /*
         This is the inifinite scrolling setting, you can modify this at your will
         */
        var inf_scrolling = {
            /*
             img: is the loading image path, add a nice gif loading icon there                    
             msgText: the loading message                
             finishedMsg: the finished loading message
             */
            loading: {
                img: "<? echo get_template_directory_uri(); ?>/images/ajax-loading.gif",
                msgText: "Loading next posts....",
                finishedMsg: "Posts loaded!!",
            },
            /*Next item is set nextSelector. 
             NextSelector is the css class of page navigation.
             In our case is #nav-below .nav-previous a
             */
            "nextSelector": "#nav-below .nav-previous a",
            //navSelector is a css id of page navigation
            "navSelector": "#nav-below",
            //itemSelector is the div where post is displayed
            "itemSelector": ".images",
            //contentSelector is the div where page content (posts) is displayed
            "contentSelector": "#content"
        };

        /*
         Last thing to do is configure contentSelector to infinite scroll,
         with a function jquery from infinite-scroll.min.js
         */
        jQuery(inf_scrolling.contentSelector).infinitescroll(inf_scrolling);
    </script>        
    <?php
    //}
}

/*
  we need to add this action on page's footer.
  100 is a priority number that correpond a later execution.
 */
add_action('wp_footer', 'set_infinite_scrolling', 100);

add_filter('init', 'add_author_more_query_var');

function add_author_more_query_var() {
    global $wp;
    $wp->add_query_var('q');
}
?>

