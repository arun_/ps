<?php

// Enqueue Flexslider Files

function t9l_slider_scripts() {
    wp_enqueue_script('jquery');

    wp_enqueue_style('flex-style', get_template_directory_uri() . '/inc/slider/css/flexslider.css');

    wp_enqueue_script('flex-script', get_template_directory_uri() . '/inc/slider/js/jquery.flexslider.js', array('jquery'), false, true);
}

add_action('wp_enqueue_scripts', 't9l_slider_scripts');

// Initialize Slider

function t9l_slider_initialize() {
    ?>
    <script>
        jQuery(document).ready(function() {
            jQuery('.flexslider').flexslider({
                slideshow: true,
                slideshowSpeed: 3000,
                animation: "fade",
                animationSpeed: 600,
                pauseOnHover: true,
                controlNav: false,
                directionNav: true,
                prevText: "",
                nextText: ""
            });
        });
    </script>
    <?php
}

add_action('wp_footer', 't9l_slider_initialize');

// Create Slider

function t9l_slider_template() {

    // Query Arguments
    $args = array(
        'post_type' => 'slides',
        'posts_per_page' => 5
    );

    // The Query
    $the_query = new WP_Query($args);

    // Check if the Query returns any posts
    if ($the_query->have_posts()) {

        // Start the Slider 
        ?>   
        <!--Banner-->
<div class="thm-container">
	<div class="clearfix">
    	<div class="homeslider">
               <div class="accordionContainer">
                  <ul>
					  <?php
                // The Loop
                while ($the_query->have_posts()) : $the_query->the_post();
                    $image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), '');
                    $metaslider = get_post_meta(get_the_ID());
                    $slidetext = "";
                    if (!empty($metaslider['t9l_slideurl'])) {
                        $slidetext = $metaslider['t9l_slidetext'][0];
                    }
                    ?>  
                     <li class="hs-1">
                      <h1 class="title"><?php echo get_the_title(get_the_ID()); ?></h1>
                      <div class="content">
                      	<img src="<?php echo $image[0]; ?>" />
                      </div>
                    </li>                    
                    <!--End Banner-->
                <?php endwhile; ?>                                      
                  </ul>
                </div>
            </div>
    </div>
</div>
        <?php
    }

    // Reset Post Data
    wp_reset_postdata();
}

// Slider Shortcode

function rc_slider_shortcode() {
    ob_start();
    t9l_slider_template();
    $slider = ob_get_clean();
    return $slider;
}

add_shortcode('slider', 'rc_slider_shortcode');
?>
