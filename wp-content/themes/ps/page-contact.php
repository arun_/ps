<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Example
 */
get_header();
?>
<section class="inner-banner">
    <div class="thm-container clearfix">
        <h2 class="pull-left">Contact Us</h2>
        <ul class="breadcumb pull-right">
            <li><a href="/"><i class="fa fa-home"></i> Home</a></li>
            <li><span>Contact Us</span></li>
        </ul>
    </div>
</section>
<section class="inner-call-to-action">
    <div class="thm-container clearfix">
        <div class="inner-wrapper clearfix">
            <div class="left-text pull-left">
                <h3><span>24/7</span> For Technical Support Call  <span>+91 9654 676 910</span></h3>
            </div>
            <div class="right-button pull-right">
                <a href="/request-quote" class="thm-btn">Make Appointment <i class="fa fa-arrow-right"></i></a>
            </div>
        </div>
    </div>
</section>

<section class="sec-padding contact-page-content bottom-has-footer-top">
    <div class="thm-container">
        <div class="sec-title">
            <h2><span>Get in touch</span></h2>
            <p>You can talk to our online representative at any time. Please use our Live Chat System on our website or <br>Fill up below instant messaging programs.</p>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="contact-info">
                    <ul>
                        <li>
                            <div class="icon-box">
                                <i class="fa fa-map-marker"></i>
                            </div>
                            <div class="content">
                                <p>WZ 74A, Ramgarh colony, Street no 9, Near Moti Nagar 110015</p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4">
                <div class="contact-info">
                    <ul>
                        <li>
                            <div class="icon-box">
                                <i class="fa fa-envelope-o"></i>
                            </div>
                            <div class="content">
                                <p>Email Us<br />info@puncturesolution.co.in </p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4">
                <div class="contact-info">
                    <ul>
                        <li>
                            <div class="icon-box">
                                <i class="fa fa-phone"></i>
                            </div>
                            <div class="content">
                                <p>Call Now<br />+91 9654 676 910</p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-7 col-sm-6 col-xs-12 pull-left">
                <form action="" class="contact-form contact-page">
                    <p><input type="text" placeholder="Name" name="name"></p>
                    <p><input type="text" placeholder="Email" name="email"></p>
                    <p><input type="text" placeholder="Subject" name="subject"></p>
                    <p><textarea name="message" placeholder="Message"></textarea></p>
                    <button type="submit" class="thm-btn">Submit Now <i class="fa fa-arrow-right"></i></button>
                </form>
            </div>
            <div class="col-md-4 hidden-sm text-center pull-right">
                <img src="<?php echo get_bloginfo('template_url'); ?>/images/request-qoute-man.png" alt="Awesome Image"/>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">				
                <div class="google-map" id="contact-page-google-map" data-map-lat="28.6556637" data-map-lng="77.1325319" data-map-zoom="10" data-map-title="Puncture Solution PVT. LTD."></div>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>
