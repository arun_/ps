<?php

/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 */
get_header();
?>
<section class="inner-call-to-action">
    <div class="thm-container clearfix">
        <div class="inner-wrapper clearfix">
            <div class="left-text pull-left">
                <h3><span>24/7</span> For Technical Support Call  <span>+91 9654 676 910</span></h3>
            </div>
            <div class="right-button pull-right">
                <a href="contact.html" class="thm-btn">Make Appointment <i class="fa fa-arrow-right"></i></a>
            </div>
        </div>
    </div>
</section>

<section class="sec-padding blog-page single-post-page ">
    <div class="thm-container">
        <div class="row">			
            <div class="col-md-12">
                <div class="sec-title">
                    <h2><span>Featured Products</span></h2>
                </div>
                <div class="row">
                    <div class="col-md-4 col-sm-6">
                        <div class="single-product-item img-cap-effect">
                            <div class="img-box">
                                <img src="<?php echo get_bloginfo('template_url'); ?>/images/products/250ML.JPG" alt=""/>
                                <div class="img-caption">
                                    <div class="box-holder">
                                        <ul>
                                            <li><a href="#"><i class="fa fa-link"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <h3>PunctureSolution – 250ML</h3>
                            <p class="price">390.00 MRP</p>

                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="single-product-item img-cap-effect">
                            <div class="img-box">
                                <img src="<?php echo get_bloginfo('template_url'); ?>/images/products/500ML1.JPG" alt=""/>
                                <div class="img-caption">
                                    <div class="box-holder">
                                        <ul>
                                            <li><a href="#"><i class="fa fa-link"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <h3>PunctureSolution – New 500ML</h3>
                            <p class="price">690.00 MRP</p>

                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="single-product-item img-cap-effect">
                            <div class="img-box">
                                <img src="<?php echo get_bloginfo('template_url'); ?>/images/products/500ML.JPG" alt=""/>
                                <div class="img-caption">
                                    <div class="box-holder">
                                        <ul>
                                            <li><a href="#"><i class="fa fa-link"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <h3>PunctureSolution – 500ML</h3>
                            <p class="price">690.00 MRP</p>

                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="single-product-item img-cap-effect">
                            <div class="img-box">
                                <img src="<?php echo get_bloginfo('template_url'); ?>/images/products/1LTR.JPG" alt=""/>
                                <div class="img-caption">
                                    <div class="box-holder">
                                        <ul>
                                            <li><a href="#"><i class="fa fa-link"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <h3>PunctureSolution – 1  LITRES</h3>
                            <p class="price">1100.00 MRP</p>

                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="single-product-item img-cap-effect">
                            <div class="img-box">
                                <img src="<?php echo get_bloginfo('template_url'); ?>/images/products/20l.JPG" alt=""/>
                                <div class="img-caption">
                                    <div class="box-holder">
                                        <ul>
                                            <li><a href="#"><i class="fa fa-link"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <h3>PunctureSolution – 20 LITRES</h3>
                            <p class="price">20099.00 MRP</p>

                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="single-product-item img-cap-effect">
                            <div class="img-box">
                                <img src="<?php echo get_bloginfo('template_url'); ?>/images/products/100ml1000.JPG" alt=""/>
                                <div class="img-caption">
                                    <div class="box-holder">
                                        <ul>
                                            <li><a href="#"><i class="fa fa-link"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <h3>PunctureSolution – 100 ML.</h3>
                            <p class="price">1000.00 MRP</p>

                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="single-product-item img-cap-effect">
                            <div class="img-box">
                                <img src="<?php echo get_bloginfo('template_url'); ?>/images/products/100ml1000.JPG" alt=""/>
                                <div class="img-caption">
                                    <div class="box-holder">
                                        <ul>
                                            <li><a href="#"><i class="fa fa-link"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <h3>PunctureSolution – 200 ML.</h3>
                            <p class="price">1300.00 MRP</p>

                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="single-product-item img-cap-effect">
                            <div class="img-box">
                                <img src="<?php echo get_bloginfo('template_url'); ?>/images/products/50ml.JPG" alt=""/>
                                <div class="img-caption">
                                    <div class="box-holder">
                                        <ul>
                                            <li><a href="#"><i class="fa fa-link"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <h3>PunctureSolution – 50ML Mini Pump for Bottles.</h3>
                            <p class="price">NA</p>

                        </div>
                    </div>
                </div>				
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>
