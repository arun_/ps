<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Example
 */
get_header();
?>
  <section class="inner-banner">
	<div class="thm-container clearfix">
		<h2 class="pull-left">About Us</h2>
		<ul class="breadcumb pull-right">
			<li><a href="index.html"><i class="fa fa-home"></i> Home</a></li>
			<li><span>About Us</span></li>
		</ul>
	</div>
</section>

<section class="inner-call-to-action">
	<div class="thm-container clearfix">
		<div class="inner-wrapper clearfix">
			<div class="left-text pull-left">
				<h3><span>24/7</span> For Technical Support Call  <span>+91 9654 676 910</span></h3>
			</div>
			<div class="right-button pull-right">
				<a href="contact.html" class="thm-btn">Contact Us <i class="fa fa-arrow-right"></i></a>
			</div>
		</div>
	</div>
</section>
<?php if ( have_posts() ) : ?>

			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php the_content(); ?>

			<?php endwhile; ?>

			<?php the_posts_navigation(); ?>

		<?php else : ?>

			<?php get_template_part( 'template-parts/content', 'none' ); ?>

		<?php endif; ?>
<?php get_footer(); ?>
