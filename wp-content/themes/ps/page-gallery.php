<?php

/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 */
get_header();
?>
<section class="inner-banner">
    <div class="thm-container clearfix">
        <h2 class="pull-left">Gallery</h2>
        <ul class="breadcumb pull-right">
            <li><a href="index.html"><i class="fa fa-home"></i> Home</a></li>
            <li><span>Our Product Gallery</span></li>
        </ul>
    </div>
</section>

<section class="inner-call-to-action">
    <div class="thm-container clearfix">
        <div class="inner-wrapper clearfix">
            <div class="left-text pull-left">
                <h3><span>24/7</span> For Technical Support Call  <span>+91 9654 676 910</span></h3>
            </div>
            <div class="right-button pull-right">
                <a href="contact.html" class="thm-btn">Make Appointment <i class="fa fa-arrow-right"></i></a>
            </div>
        </div>
    </div>
</section>

<section class="fleet-gallery  gray-bg sec-padding">
    <div class="thm-container">
        <div class="clearfix single-fleet-gallery-wrapper">
            <div class="col-md-4 col-sm-6 col-xs-12 mix ground">
                <div class="single-fleet-gallery img-cap-effect">
                    <div class="img-box">
                        <img src="images/gallery/thumb-1.jpg" alt=""/>
                        <div class="img-caption">
                            <div class="box-holder">
                                <div class="box-caption">									
                                    <ul>
                                        <li><a href="images/gallery/g1.JPG" class="fancybox"><i class="fa fa-search-plus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12 mix ground">
                <div class="single-fleet-gallery img-cap-effect">
                    <div class="img-box">
                        <img src="images/gallery/thumb-2.jpg" alt=""/>
                        <div class="img-caption">
                            <div class="box-holder">
                                <div class="box-caption">									
                                    <ul>
                                        <li><a href="images/gallery/g2.JPG" class="fancybox"><i class="fa fa-search-plus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12 mix ground">
                <div class="single-fleet-gallery img-cap-effect">
                    <div class="img-box">
                        <img src="images/gallery/thumb-3.jpg" alt=""/>
                        <div class="img-caption">
                            <div class="box-holder">
                                <div class="box-caption">									
                                    <ul>
                                        <li><a href="images/gallery/g3.JPG" class="fancybox"><i class="fa fa-search-plus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12 mix ground">
                <div class="single-fleet-gallery img-cap-effect">
                    <div class="img-box">
                        <img src="images/gallery/thumb-4.jpg" alt=""/>
                        <div class="img-caption">
                            <div class="box-holder">
                                <div class="box-caption">									
                                    <ul>
                                        <li><a href="images/gallery/g4.JPG" class="fancybox"><i class="fa fa-search-plus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12 mix ground">
                <div class="single-fleet-gallery img-cap-effect">
                    <div class="img-box">
                        <img src="images/gallery/thumb-5.jpg" alt=""/>
                        <div class="img-caption">
                            <div class="box-holder">
                                <div class="box-caption">									
                                    <ul>
                                        <li><a href="images/gallery/g5.JPG" class="fancybox"><i class="fa fa-search-plus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12 mix ground">
                <div class="single-fleet-gallery img-cap-effect">
                    <div class="img-box">
                        <img src="images/gallery/thumb-6.jpg" alt=""/>
                        <div class="img-caption">
                            <div class="box-holder">
                                <div class="box-caption">									
                                    <ul>
                                        <li><a href="images/gallery/g6.JPG" class="fancybox"><i class="fa fa-search-plus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12 mix ground">
                <div class="single-fleet-gallery img-cap-effect">
                    <div class="img-box">
                        <img src="images/gallery/thumb-7.jpg" alt=""/>
                        <div class="img-caption">
                            <div class="box-holder">
                                <div class="box-caption">									
                                    <ul>
                                        <li><a href="images/gallery/g7.JPG" class="fancybox"><i class="fa fa-search-plus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12 mix ground">
                <div class="single-fleet-gallery img-cap-effect">
                    <div class="img-box">
                        <img src="images/gallery/thumb-8.jpg" alt=""/>
                        <div class="img-caption">
                            <div class="box-holder">
                                <div class="box-caption">									
                                    <ul>
                                        <li><a href="images/gallery/g8.JPG" class="fancybox"><i class="fa fa-search-plus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>			
        </div>
    </div>
</section>
<?php get_footer(); ?>