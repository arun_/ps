<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'pc');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '$VmK0#tBy=gT}YnGvN:obhO d~Hn[OOz+/$f]jZxu*|;9d7,yXY2)sfA02Y+?Nz2');
define('SECURE_AUTH_KEY',  '-+,@NMTmLNA[0-E*HILq#?Tj1Z*#@$m_Y,F|#!65Jhe)jb w}[hJWBAV3GIjWS@O');
define('LOGGED_IN_KEY',    '3s!KNC8,s#5rrJH~Nenj_oxQyH`^0?Q&YpJMHV#5O3MK7bK73IKvzcdDgO<nx2Lk');
define('NONCE_KEY',        '1V +M(A3-$[:0Ob~[mVVp6Npr}$P-YiZcANK$r~e%5xIsFD%Q?z9Fy3-)XMCR{6G');
define('AUTH_SALT',        '@[*(ZnSz?*H{&[>Ix2#R3kPbdCu}F-|sJa_Ls~<*@@llB5CG}-73u*J}Bc65xWiU');
define('SECURE_AUTH_SALT', 'eXlWkHW8uCl*5Sn[Tk`DSh-#ve7$2;LVSN,OBx8rNib8(U1.U50lx[/,uu31md$_');
define('LOGGED_IN_SALT',   '3W$2]LM7dy5=n=|~q9nO#CUeYt:M71snS@gN,d37 Tf**^|3#37q^KW]naSQNRed');
define('NONCE_SALT',       '}Q~r(k)^$R1{B&VrkYnvA9O:q{9>9- ;jt4 O!9R8Fsd>=ZV+5x0|RdJ:RKJw+=k');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
